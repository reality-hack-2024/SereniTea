using UnityEngine;
using UnityEngine.Events;

public class ColliderEventsTrigger : MonoBehaviour
{
    [Header("Events")]
    public UnityEvent onTriggerEnterEvent;
    public UnityEvent onTriggerStayEvent;
    public UnityEvent onTriggerExitEvent;

    private void OnTriggerEnter(Collider other)
    {
        onTriggerEnterEvent.Invoke();
    }

    private void OnTriggerStay(Collider other)
    {
        onTriggerStayEvent.Invoke();
    }

    private void OnTriggerExit(Collider other)
    {
        onTriggerExitEvent.Invoke();
    }
}
