using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; // For UI elements like Button and Image
using UnityEngine.EventSystems; // For event handling
using UnityEngine.Events;

public class storyboard : MonoBehaviour
{
     [Header("Game State Events")]
    public List<UnityEvent> gameStateEvents = new List<UnityEvent>();

    [System.Serializable]
    public class StepSet
    {

        public AudioClip stepAudio;

        public bool autoplayNextStepAfterAudio; 
    }

    public List<StepSet> steps = new List<StepSet>();
    private int currentStep = 0;
    private AudioSource audioSource;
    

    void Start()
    {
        audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.volume = 1.0f;
        InitializeStep(currentStep);
    }

    public void InitializeStep(int stepIndex)
    {
        if (stepIndex < 0 || stepIndex >= steps.Count) return;

        StepSet currentStepSet = steps[stepIndex];
        // Play audio if it exists
        if (currentStepSet.stepAudio != null)
        {
            audioSource.clip = currentStepSet.stepAudio;
            audioSource.Play();
            if (currentStepSet.autoplayNextStepAfterAudio)
            {
                StartCoroutine(WaitForAudioToEnd(currentStepSet.stepAudio.length));
            }
        }
        if (stepIndex < gameStateEvents.Count)
        {
            gameStateEvents[stepIndex].Invoke();
        }

    }
    private IEnumerator WaitForAudioToEnd(float audioLength)
    {
        yield return new WaitForSeconds(audioLength);

        NextStep(); // Proceed to the next step after the audio finishes playing
    }
    public void NextStep()
    {

        currentStep++;

        if (currentStep >= steps.Count)
        {
            Debug.Log("Reached the end of the storyboard");
            return;
        }

         InitializeStep(currentStep);
    }

    
}
